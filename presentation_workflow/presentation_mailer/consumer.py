import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval():
    send_mail(
        "Your presentation has been accepted",
        "{presenter_name}, we're happy to tell you that your presentation {title} has been accepted",
        "admin@conference.go",
        ["presenter_email"],
        fail_silently=False,
    )


def process_message(ch, method, properties, body):
    print("  Received %r" % body)
    process_approval()
    print("email send")


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_message,
    auto_ack=True,
)

channel.start_consuming()
