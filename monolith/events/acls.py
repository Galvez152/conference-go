import requests
from .keys import Pexels_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": Pexels_API_KEY}
    params = {"query": f"{city} {state}", "per_page": 1}
    # Create the URL for the request with the city and state

    res = requests.get(url, params=params, headers=headers)
    pexel_dict = res.json()
    picture_url = pexel_dict["photos"][0]["src"]["original"]
    return {"picture_url": picture_url}

    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_lan_lon(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},USA",
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    res = requests.get(
        url,
        params=params,
    )
    the_json = res.json()
    lat = the_json[0]["lat"]
    lon = the_json[0]["lon"]
    return lat, lon


def get_weather(city, state):
    url = "https://api.openweathermap.org/data/2.5/weather"
    lat, lon = get_lan_lon(city, state)
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    res = requests.get(url, params=params)
    the_json = res.json()
    temp = the_json["main"]["temp"]
    description = the_json["weather"][0]["description"]
    return {"temp": temp, "description": description}
